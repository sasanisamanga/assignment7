#include <stdio.h>
void data(int first[][10], int second[][10], int row1, int column1, int row2, int column2);
void multiply(int first[][10], int second[][10], int Result[][10], int row1, int column1, int row2, int column2);
void display(int mult[][10], int row1, int column2);

int main() {
    int first[10][10], second[10][10], mult[10][10], row1, column1,row2, column2;
    printf("Enter rows and column for the first matrix: ");
    scanf("%d %d", &row1, &column1);
    printf("Enter rows and column for the second matrix: ");
    scanf("%d %d", &row2, &column2);


    while (column1 != row2) {
        printf("Error! Enter rows and columns again.\n");
        printf("Enter rows and columns for the first matrix: ");
        scanf("%d%d", &row1, &column1);
        printf("Enter rows and columns for the second matrix: ");
        scanf("%d%d", &row2, &column2);
    }


    data(first, second, row1, column1, row2, column2);


    multiply(first, second, mult, row1, column1, row2, column2);


    display(mult, row1, column2);

    return 0;
}

void data(int first[][10], int second[][10], int row1, int column1, int row2, int column2) {
    printf("\nEnter elements of matrix 1:\n");


    for (int i = 0; i < row1; ++i) {
        for (int j = 0; j < column1; ++j) {
            printf("Enter %d.%d: ", i + 1, j + 1);
            scanf("%d", &first[i][j]);
        }
    }
    printf("\nEnter elements of matrix 2:\n");

    for (int i = 0; i < row2; ++i) {
        for (int j = 0; j < column2; ++j) {
            printf("Enter %d.%d: ", i + 1, j + 1);
            scanf("%d", &second[i][j]);
        }
    }
}

void multiply(int first[][10], int second[][10], int mult[][10], int row1, int column1, int row2, int column2) {


    for (int i = 0; i < row1; ++i) {
        for (int j = 0; j < column2; ++j) {
            mult[i][j] = 0;
        }
    }


    for (int i = 0; i < row1; ++i) {
        for (int j = 0; j < column2; ++j) {
            for (int k = 0; k < column1; ++k) {
                mult[i][j] += first[i][k] * second[k][j];
            }
        }
    }
}

void display(int mult[][10], int row1, int column2) {

    printf("\nOutput Matrix:\n");
    for (int i = 0; i < row1; ++i) {
        for (int j = 0; j < column2; ++j) {
            printf("%d  ", mult[i][j]);
            if (j == column2 - 1)
                printf("\n");
        }
    }
}
