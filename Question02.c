#include <stdio.h>
int main() {
    char str[1000], Char;
    int count = 0;

    printf("Enter a string: ");
    fgets(str, sizeof(str), stdin);

    printf("Enter a character to find its frequency: ");
    scanf("%c", &Char);

    for (int i = 0; str[i] != '\0'; ++i) {
        if (Char == str[i])
            ++count;
    }

    printf("Frequency of %c = %d", Char, count);
    return 0;
}
