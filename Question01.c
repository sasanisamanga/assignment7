#include <stdio.h>
#include <string.h>
#define MAX_SIZE 100

int main()
{
    char str[100], reverse[100];
    int length, i, index, start, end;

    printf("Enter any sentence : ");
    gets(str);

    length   = strlen(str);
    index = 0;



    start = length - 1;
    end   = length - 1;

    while(start > 0)
    {


        if(str[start] == ' ')
        {


            i = start + 1;
            while(i <= end)
            {
                reverse[index] = str[i];

                i++;
                index++;
            }
            reverse[index++] = ' ';

            end = start - 1;
        }

        start--;
    }



    for(i=0; i<=end; i++)
    {
        reverse[index] = str[i];
        index++;
    }



    reverse[index] = '\0';

    printf("\nBefore reverse the sentence : \n%s\n\n", str);
    printf("After reverse the sentence : \n%s\n", reverse);

    return 0;
}
